import {AfterViewInit, Component} from '@angular/core';
import {Sortable, Plugins} from '@shopify/draggable';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements AfterViewInit {

  sortable: Sortable;
  containers;
  containerOne;
  containerTwo;

  ngAfterViewInit() {
    this.containers = document.querySelectorAll('.containerOne,.containerTwo');
    this.sortable = new Sortable(this.containers, {
      draggable: 'li',
      mirror: {
        constrainDimensions: true
      },
      plugins: [Plugins.ResizeMirror]
    });

    this.containerOne = this.containers[0];
    this.containerTwo = this.containers[1];

    this.sortable.on('drag:start', (evt) => {
      // console.log('drag:start', evt);
    });

      // this.sortable.on('sortable:sort', (evt) => {
      //   // console.log('sortable:sort', evt);
      //   if (evt.dragEvent.overContainer === this.containerOne && evt.dragEvent.sourceContainer !== this.containerOne) {
      //     // console.log('getIntoContainerOne');
      //     const mirrorDragTable =  document.querySelectorAll('.widget-container.draggable-mirror > .onDragTable');
      //     // console.log('mirrorElement', mirrorElement);
      //     mirrorDragTable[0].classList.add('d-block');
      //   } else if (evt.dragEvent.sourceContainer === this.containerOne && evt.dragEvent.overContainer !== this.containerOne) {
      //     // console.log('goOutContainerOne');
      //   }
      // });
      //
      // this.sortable.on('sortable:stop', (evt) => {
      //   if (evt.newContainer === this.containerOne) {
      //     evt.dragEvent.originalSource.getElementsByClassName('onDragTable').item(0).classList.add('d-block');
      //   } else if (evt.oldContainer === this.containerOne){
      //     evt.dragEvent.originalSource.getElementsByClassName('onDragTable').item(0).classList.remove('d-block');
      //   }
      // });



  }
}
