import {Component, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-custom-accessor',
  templateUrl: './custom-accessor.component.html',
  styleUrls: ['./custom-accessor.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef( () => CustomAccessorComponent),
    multi: true
  }]
})
export class CustomAccessorComponent implements ControlValueAccessor{
  inputValue = 0;
  private onChange = (value: any) => {};
  private onTouched = () => {};

  registerOnChange(callback: (change: any) => void): void {
    this.onChange = callback;
  }

  registerOnTouched(callback: () => void): void {
    this.onTouched = callback;
  }

  writeValue(value: any): void {
    this.inputValue = value;
  }

  updateValue(insideValue: any) {
    this.inputValue = insideValue;
    this.onChange(insideValue);
    this.onTouched();
  }

}
