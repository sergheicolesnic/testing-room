import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {WidgetComponent} from './components/widget/widget.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CustomAccessorComponent} from './components/custom-accessor/custom-accessor.component';
import {SortableDirective} from './sortable.directive';

@NgModule({
  declarations: [
    AppComponent,
    WidgetComponent,
    CustomAccessorComponent,
    SortableDirective
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
